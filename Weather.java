package task1;

import java.io.FileWriter;
import java.io.IOException;

public class Weather {
    public WeatherContainer query;
    private Weather test;

    public Weather(String city) throws Exception {
        Downloader request = new Downloader();
        test = request.sendGet(city);
    }

    public void print() throws IOException {
        FileWriter printer = new FileWriter("E:\\Java\\src\\task1\\weather.csv", false);

        for ( WeatherContainer.Results.Channel v : test.query.getResults().getChannel()){
            printer.write(v.getItem().getForecast().getDay()+";"+
                    v.getItem().getForecast().getDate()+";"+
                    v.getItem().getForecast().getLow()+";"+
                    v.getItem().getForecast().getMaxTempCel()+";"+
                    v.getItem().getForecast().getMinTempCel());
            printer.write(System.lineSeparator());
        }

        printer.flush();
    }
}
