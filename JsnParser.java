package task1;

import com.google.gson.*;

public class JsnParser {
    public Weather parseRequest(String request) {
        Gson gson = new GsonBuilder().create();
        Weather tmp = gson.fromJson(request, Weather.class);
        return tmp;
    }
}
