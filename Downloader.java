package task1;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class Downloader {
    //select item.forecast from weather.forecast where woeid in (select woeid from geo.places(1) where text="")
    private final String USER_AGENT = "Mozilla/5.0";

    public Weather sendGet(String city) throws Exception {
        //take request url form file url.txt
        FileReader file = new FileReader("E:\\Java\\src\\task1\\url.txt");
        Scanner scan = new Scanner(file);
        StringBuffer url = new StringBuffer(scan.nextLine());
        file.close();
        url.insert(url.indexOf("%3D%22")+6,city);

        //send GET request
        URL obj = new URL(url.toString());
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //parse request
        JsnParser parser = new JsnParser();
        return parser.parseRequest(response.toString());
    }
}
